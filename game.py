# -*- coding: utf-8 -*-
from chessboard import Chessboard
from os import system
from tools.notation import extract_notation_information
import re
from copy import deepcopy
from tools.colors import Color
from pieces import Pawn

class Game:
  def __init__(self):
    self.player_to_move = Color.WHITE
    self.playing = False
    self.half_moves = 0
    self.history = []

  @property
  def chessboard(self):
    return self.history[-1]

  def start(self):
    self.player_to_move = Color.WHITE
    self.playing = True
    self.half_moves = 0
    self.history = []
    self.history.append(Chessboard())
    system('clear')
    while(self.playing):
      print(self.chessboard)
      _input = input(f'{self.player_to_move.value} to move (q to quit): ')
      if _input == 'q':
        self.playing = False
        break
      else:
        self.move(_input)

  def move(self, notation):
    self.history.append(deepcopy(self.chessboard))
    moving_color = self.player_to_move

    system('clear')
    try:
      move_info = extract_notation_information(notation)
    except:
      print('That is not a valid move!')
      return
    # Find all eligible pieces. Piece is eligible if:
    # It's the correct color
    # It's the correct type
    # It can move to the target square.
    if 'castles' in move_info:
      king = self.chessboard.get_pieces(piece_class=move_info['piece'], color=self.player_to_move)[0]
      if king.can_castle(self.chessboard, move_info['castle_side']):
        king.castle(self.chessboard, move_info['castle_side'])
        self.player_to_move = Color.WHITE if self.player_to_move == Color.BLACK else Color.BLACK
        self.half_moves += 1
    else:
      eligible_pieces = [
        piece for piece in self.chessboard.get_pieces(piece_class=move_info['piece'], color=self.player_to_move) \
        if move_info['square'] in piece.valid_moves(self.chessboard, move_info['capture'], move_info['promoting_class'])
      ]
      if len(eligible_pieces) == 0:
        print('That is not a valid move')
      elif len(eligible_pieces) > 1:
        eligible_pieces = [ eligible_piece for eligible_piece in eligible_pieces if move_info['specifier'] in self.chessboard.get_square(eligible_piece)]
        if len(eligible_pieces) == 0:
          print('That is not a valid move')
        elif len(eligible_pieces) > 1:
          print('Multiple pieces can make that move. Please try another specifier')
        else:
          eligible_pieces[0].move(self.chessboard, move_info['square'], move_info['promoting_class'])
          self.player_to_move = self.player_to_move.next
          self.half_moves += 1
      else:
        eligible_pieces[0].move(self.chessboard, move_info['square'], move_info['promoting_class'])
        self.player_to_move = self.player_to_move.next
        self.half_moves += 1

    if self.chessboard.is_check(moving_color):
      print('That move would leave you in check.')
      self.chessboard = board_before_move
      self.player_to_move = moving_color
      self.half_moves -= 1
      self.history.pop()
    else:
      if move_info['piece'] == Pawn or move_info['capture']:
        self.half_moves = 0

    if self.chessboard.is_checkmate(self.player_to_move):
      print('Checkmate.')
      self.playing = False
    elif self.chessboard.is_stalemate(self.player_to_move):
      print('Stalemate.')
      self.playing = False
    elif self.half_moves >= 100:
      print('Draw by 50 move rule')
      self.playing = False
    if len(list(filter(lambda x: x == self.chessboard, self.history))) >= 3:
      print('Draw by threefold repetition')
      self.playing = False


if __name__ == '__main__':
  Game().start()
