# -*- coding: utf-8 -*-
def square_to_coords(square):
  rank = int(square[1])
  file = 'abcdefgh'.index(square[0]) + 1
  return rank, file

def coords_to_square(rank, file):
  _file = 'abcdefgh'[file-1]
  _rank = str(rank)
  return _file + _rank
