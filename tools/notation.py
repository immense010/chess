# -*- coding: utf-8 -*-
import re
from pieces import Rook, Knight, Bishop, Queen, King
from pieces.tools.conversion import convert_piece_to_class

def extract_notation_information(notation):
  # Should return:
    # piece class
    # specifier
    # capture
    # square
    # promoting class
  move_regex = re.compile('(?P<piece>[RNBQKa-h])?(?P<specifier>[a-h1-8])?(?P<capture>x)?(?P<square>[a-h][1-8])(=(?P<promoting_class>[RNBQ]))?')
  match = move_regex.match(notation)
  if match != None:
    #This is a regular move
    information = match.groupdict()
    information['piece'] = convert_piece_to_class(information['piece'])
    information['capture'] = information['capture'] == 'x'
    information['promoting_class'] = convert_piece_to_class(information['promoting_class'])
    if information['promoting_class'] not in [Rook, Knight, Bishop, Queen]:
      information['promoting_class'] = None
  elif notation in ('0-0', 'O-O'):
    information = {
      'piece': King,
      'capture': False,
      'promoting_class': None,
      'castles': True,
      'castle_side': 'kingside',
    }
  elif notation in ('0-0-0', 'O-O-O'):
    information = {
      'piece': King,
      'capture': False,
      'promoting_class': None,
      'castles': True,
      'castle_side': 'queenside',
    }
  else:
    raise Exception('Invalid Move')
  return information