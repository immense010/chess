# -*- coding: utf-8 -*-
from enum import Enum

class Color(Enum):
  WHITE = 'white'
  BLACK = 'black'

  @property
  def next(self):
    return self.WHITE if self == Color.BLACK else self.BLACK