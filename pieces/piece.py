# -*- coding: utf-8 -*-
class Piece:
  def __init__(self, color):
    self.color = color

  def __eq__(self, other):
    return self.__class__ == other.__class__ and self.color == other.color

  def valid_moves(self, chessboard, square):
    raise NotImplementedError()

  def valid_moves(self, chessboard, is_capture, promoting_class = None):
    if is_capture:
      return self.valid_capturing_moves(chessboard, promoting_class)
    return self.valid_non_capturing_moves(chessboard, promoting_class)

  def valid_capturing_moves(self, chessboard, promoting_class = None):
    raise NotImplementedError()

  def valid_non_capturing_moves(self, chessboard, promoting_class = None):
    raise NotImplementedError()

  def move(self, chessboard, square, promoting_class = None):
    current_square = chessboard.get_square(self)
    del chessboard.board[current_square]
    chessboard.board[square] = self
    chessboard.en_passant_square = None
    chessboard.en_passant_target = None

  def can_capture(self, chessboard, square):
    return square in self.valid_capturing_moves(chessboard, None) or square in self.valid_non_capturing_moves(chessboard, None) # Shouldn't need None here...

  def available_squares(self, chessboard):
    return set(self.valid_moves(chessboard, True, None) + (self.valid_moves(chessboard, False, None)))