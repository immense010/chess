# -*- coding: utf-8 -*-
from .piece import Piece
from .queen import Queen
from tools.conversion import coords_to_square, square_to_coords
from tools.colors import Color

class Pawn(Piece):

  def __init__(self, color):
    super().__init__(color)
    self.has_moved = False

  def __str__(self):
    return '♙' if self.color == Color.BLACK else '♟'

  def valid_non_capturing_moves(self, chessboard, promoting_class):
    direction = 1 if self.color == Color.WHITE else -1
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    values = []
    # Moving forward
    square_to_check = coords_to_square(current_rank+1*direction, current_file)
    if square_to_check not in chessboard.board:
      if not (square_to_check[1] in '18') ^ (promoting_class != None):
        values.append(square_to_check)
      square_to_check = coords_to_square(current_rank+2*direction, current_file)
      if square_to_check not in chessboard.board:
        if not (square_to_check[1] in '18') ^ (promoting_class != None):
          values.append(square_to_check)
    return values

  def valid_capturing_moves(self, chessboard, promoting_class):
      direction = 1 if self.color == Color.WHITE else -1
      current_square = chessboard.get_square(self)
      current_rank, current_file = square_to_coords(current_square)
      values = []
      for file_direction in (-1,1):
        try:
          square_to_check = coords_to_square(current_rank+1*direction, current_file + file_direction)
          if square_to_check in chessboard.board and chessboard.board[square_to_check].color != self.color:
            if not (square_to_check[1] in '18') ^ (promoting_class != None):
              values.append(square_to_check)
          elif square_to_check == chessboard.en_passant_square:
            if not (square_to_check[1] in '18') ^ (promoting_class != None):
              values.append(square_to_check)
        except:
          pass
      return values

  def can_capture(self, chessboard, square):
    direction = 1 if self.color == Color.WHITE else -1
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    for file_direction in (-1,1):
      try:
        if square == coords_to_square(current_rank+1*direction, current_file + file_direction):
          return True
      except:
        pass
    return False

  def move(self, chessboard, square, promoting_class = None):
    starting_rank, starting_file = square_to_coords(chessboard.get_square(self))
    if square == chessboard.en_passant_square:
      del chessboard.board[chessboard.get_square(chessboard.en_passant_target)]
    super().move(chessboard, square)
    # Check if moving 2 squares, and if so set the square behind me to the en passant square
    target_rank = square_to_coords(square)[0]
    if abs(starting_rank - target_rank) > 1:
      chessboard.en_passant_square = coords_to_square(int((starting_rank + target_rank)/2), starting_file)
      chessboard.en_passant_target = self
    # Promote if needed
    if target_rank in (1,8) and promoting_class != None:
      chessboard.board[square] = promoting_class(self.color)
    self.has_moved = True

  def can_capture(self, chessboard, square):
    starting_rank, starting_file = square_to_coords(chessboard.get_square(self))
    direction = 1 if self.color == Color.WHITE else -1
    target_rank, target_file = square_to_coords(square)
    return target_rank == starting_rank + 1 * direction and target_file in (starting_file + 1, starting_file - 1)

  def available_squares(self, chessboard):
    squares = []
    squares += self.valid_capturing_moves(chessboard, None )
    squares += self.valid_capturing_moves(chessboard, Queen )
    squares += self.valid_non_capturing_moves(chessboard, None )
    squares += self.valid_non_capturing_moves(chessboard, Queen )
    return set(squares)