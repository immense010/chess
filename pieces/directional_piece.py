# -*- coding: utf-8 -*-
from .piece import Piece
from tools.conversion import coords_to_square, square_to_coords

class Directional_Piece(Piece):
  directions = []

  def valid_non_capturing_moves(self, chessboard, promoting_class):
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    values = []
    for direction in self.directions:
      target_rank, target_file = current_rank + direction[0], current_file + direction[1]
      while target_rank <= 8 and target_rank > 0 \
        and target_file <= 8 and target_file > 0 \
        and coords_to_square(target_rank, target_file) not in chessboard.board:
        values.append(coords_to_square(target_rank, target_file))
        target_rank += direction[0]
        target_file += direction[1]
    return values

  def valid_capturing_moves(self, chessboard, promoting_class):
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    values = []
    for direction in self.directions:
      target_rank, target_file = current_rank + direction[0], current_file + direction[1]
      while target_rank <= 8 and target_rank > 0 \
        and target_file <= 8 and target_file > 0:
        if coords_to_square(target_rank, target_file) in chessboard.board:
          if chessboard.board[coords_to_square(target_rank, target_file)].color != self.color:
            values.append(coords_to_square(target_rank, target_file))
          break
        target_rank += direction[0]
        target_file += direction[1]
    return values