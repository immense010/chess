# -*- coding: utf-8 -*-
from .directional_piece import Directional_Piece
from tools.colors import Color


class Bishop(Directional_Piece):
  directions = [
    (-1,-1),
    (-1,1),
    (1,-1),
    (1,1),
  ]
  def __str__(self):
    return '♗' if self.color == Color.BLACK else '♝'