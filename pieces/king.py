# -*- coding: utf-8 -*-
from .piece import Piece
from tools.conversion import coords_to_square, square_to_coords
from tools.colors import Color

class King(Piece):

  directions = [
    (-1,0),
    (1,0),
    (0,-1),
    (0,1),
    (-1,-1),
    (-1,1),
    (1,-1),
    (1,1),
  ]

  def __init__(self, color):
    super().__init__(color)
    self.has_moved = False

  def __str__(self):
    return '♔' if self.color == Color.BLACK else '♚'

  def valid_non_capturing_moves(self, chessboard, promoting_class):
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    values = []
    for direction in self.directions:
      target_rank, target_file = current_rank + direction[0], current_file + direction[1]
      if target_rank <= 8 and target_rank > 0 \
        and target_file <= 8 and target_file > 0 \
        and coords_to_square(target_rank, target_file) not in chessboard.board:
        values.append(coords_to_square(target_rank, target_file))
    return values

  def valid_capturing_moves(self, chessboard, promoting_class):
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    values = []
    for direction in self.directions:
      target_rank, target_file = current_rank + direction[0], current_file + direction[1]
      if target_rank <= 8 and target_rank > 0 \
        and target_file <= 8 and target_file > 0 \
        and coords_to_square(target_rank, target_file) in chessboard.board:

        if chessboard.board[coords_to_square(target_rank, target_file)].color != self.color:
          values.append(coords_to_square(target_rank, target_file))

    return values

  def can_castle(self, chessboard, side):
    if chessboard.castles_allowed[self.color][side]:
      #Check if there are any pieces between the target rook and I.
      #If not, check that no opposing pieces can attack squares I'll move to.
      #If not, allow castling.
      rank = 1 if self.color == Color.WHITE else 8
      files = (2,3,4) if side == 'queenside' else (6,7)
      king_files = (3,4) if side == 'queenside' else (6,7)
      for file in files:
        if(coords_to_square(rank, file) in chessboard.board):
          return False
      for piece in chessboard.get_pieces(color=Color.WHITE if self.color == Color.BLACK else Color.BLACK):
        for file in king_files:
          if piece.can_capture(chessboard, coords_to_square(rank, file)):
            return False
      return True
    return False

  def castle(self, chessboard, side):
    rank = 1 if self.color == Color.WHITE else 8
    king_file = 3 if side == 'queenside' else 7
    rook_original_file = 1 if side == 'queenside' else 8
    rook_file = 4 if side == 'queenside' else 6
    self.move(chessboard, coords_to_square(rank, king_file))
    chessboard.board[coords_to_square(rank, rook_original_file)].move(chessboard, coords_to_square(rank, rook_file), None)

  def move(self, chessboard, square, promoting_class=None):
    super().move(chessboard, square)
    if not self.has_moved:
      chessboard.castles_allowed[self.color]['kingside'] = False
      chessboard.castles_allowed[self.color]['queenside'] = False
      self.has_moved = True