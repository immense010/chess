# -*- coding: utf-8 -*-
from .directional_piece import Directional_Piece
from tools.colors import Color

class Rook(Directional_Piece):
  directions = [
    (-1,0),
    (1,0),
    (0,-1),
    (0,1),
  ]

  def __init__(self, color):
    super().__init__(color)
    self.has_moved = False

  def __str__(self):
    return '♖' if self.color == Color.BLACK else '♜'

  def move(self, chessboard, square, promoting_class):
    super().move(chessboard, square)
    if not self.has_moved:
      current_square = chessboard.get_square(self)
      side = 'kingside' if current_square[0] == 'h' else 'queenside'
      chessboard.castles_allowed[self.color][side] = False
      self.has_moved = True