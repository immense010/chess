# -*- coding: utf-8 -*-
from pieces import *
def convert_piece_to_class(piece):
  return {
    'R': Rook,
    'N': Knight,
    'B': Bishop,
    'Q': Queen,
    'K': King,
    'a': Pawn,
    'b': Pawn,
    'c': Pawn,
    'd': Pawn,
    'e': Pawn,
    'f': Pawn,
    'g': Pawn,
    'h': Pawn,
    None: Pawn,
  }[piece]

def can_convert_to(piece):
  return 