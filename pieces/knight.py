# -*- coding: utf-8 -*-
from .piece import Piece
from tools.conversion import coords_to_square, square_to_coords
from tools.colors import Color

directions = [
  (-2,-1),
  (-2,1),
  (2,-1),
  (2,1),
  (-1,-2),
  (-1,2),
  (1,-2),
  (1,2),
]

class Knight(Piece):

  def __str__(self):
    return '♘' if self.color == Color.BLACK else '♞'

  def valid_non_capturing_moves(self, chessboard, promoting_class):
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    values = []
    for direction in directions:
      rank_diff, file_diff = direction
      try:
        target_rank = current_rank + rank_diff
        target_file = current_file + file_diff
        if target_rank > 0 and target_rank < 9 and target_file > 0 and target_file < 9:
          target_square = coords_to_square(target_rank, target_file)
          if target_square not in chessboard.board:
            values.append(target_square)
      except:
        pass
    return values

  def valid_capturing_moves(self, chessboard, promoting_class):
    current_square = chessboard.get_square(self)
    current_rank, current_file = square_to_coords(current_square)
    values = []
    for direction in directions:
      rank_diff, file_diff = direction
      try:
        target_rank = current_rank + rank_diff
        target_file = current_file + file_diff
        if target_rank > 0 and target_rank < 9 and target_file > 0 and target_file < 9:
          target_square = coords_to_square(target_rank, target_file)
          if target_square in chessboard.board and chessboard.board[target_square].color != self.color:
            values.append(target_square)
      except:
        pass
    return values