# -*- coding: utf-8 -*-
from pieces import *
from copy import deepcopy
from tools.colors import Color

default_board = {
  'a1': Rook(Color.WHITE),
  'b1': Knight(Color.WHITE),
  'c1': Bishop(Color.WHITE),
  'd1': Queen(Color.WHITE),
  'e1': King(Color.WHITE),
  'f1': Bishop(Color.WHITE),
  'g1': Knight(Color.WHITE),
  'h1': Rook(Color.WHITE),
  'a2': Pawn(Color.WHITE),
  'b2': Pawn(Color.WHITE),
  'c2': Pawn(Color.WHITE),
  'd2': Pawn(Color.WHITE),
  'e2': Pawn(Color.WHITE),
  'f2': Pawn(Color.WHITE),
  'g2': Pawn(Color.WHITE),
  'h2': Pawn(Color.WHITE),
  'a7': Pawn(Color.BLACK),
  'b7': Pawn(Color.BLACK),
  'c7': Pawn(Color.BLACK),
  'd7': Pawn(Color.BLACK),
  'e7': Pawn(Color.BLACK),
  'f7': Pawn(Color.BLACK),
  'g7': Pawn(Color.BLACK),
  'h7': Pawn(Color.BLACK),
  'a8': Rook(Color.BLACK),
  'b8': Knight(Color.BLACK),
  'c8': Bishop(Color.BLACK),
  'd8': Queen(Color.BLACK),
  'e8': King(Color.BLACK),
  'f8': Bishop(Color.BLACK),
  'g8': Knight(Color.BLACK),
  'h8': Rook(Color.BLACK),
}

class Chessboard:
  def __init__(self, board=default_board):
    self.board = board
    self.castles_allowed = {
      Color.WHITE: {
        'kingside': True,
        'queenside': True,
      },
      Color.BLACK: {
        'kingside': True,
        'queenside': True,
      },
    }
    self.en_passant_square = None
    self.en_passant_target = None

  def __eq__(self, other):
    return self.board == other.board \
      and self.castles_allowed == other.castles_allowed \
      and self.en_passant_square == other.en_passant_square \
      and self.en_passant_target == other.en_passant_target

  def __str__(self):
    first_row = '  _ _ _ _ _ _ _ _\n'
    last_row = '  a b c d e f g h\n'
    board_rows = ''
    for rank in range(8,0,-1):
      board_rows += f'{rank}|'
      for file in [char for char in 'abcdefgh']:
        if f'{file}{rank}' in self.board:
          board_rows += str(self.board[f'{file}{rank}']) + ' '
        else:
          board_rows += '  '
      board_rows += '|\n'
    return first_row + board_rows + last_row

  def get_pieces(self, piece_class = None, color=None):
    def is_valid(piece, _class, color):
      if(piece_class != None and type(piece) != piece_class):
        return False
      if(color != None and piece.color != color):
        return False
      return True

    return [piece for piece in self.board.values() if is_valid(piece, piece_class, color)]

  def get_square(self, piece):
    for key in self.board:
      if self.board[key] is piece:
        return key

  def get_king_position(self, color):
    return self.get_square(self.get_pieces(piece_class=King, color=color)[0])

  def is_check(self, color):
    opponent_pieces = self.get_pieces(color=color.next)
    for piece in opponent_pieces:
      if piece.can_capture(self, self.get_king_position(color)):
        return True
    return False

  def available_moves(self, color):
    moves = []
    for piece in self.get_pieces(color=color):
      squares = piece.available_squares(self)
      moves.append((piece, squares))
    return moves

  def is_checkmate(self, color):
    #Try each of my available moves. If any of them don't leave me in check, I'm not in checkmate
    if not self.is_check(color):
      return False
    for piece, squares in self.available_moves(color):
      for square in squares:
        dummy_board = deepcopy(self)
        piece_square = self.get_square(piece)
        dummy_board.board[piece_square].move(dummy_board, square, None)
        escaped_check = not dummy_board.is_check(color)
        if escaped_check:
          return False
    return True

  def is_stalemate(self, color):
    #Try each of my available moves. If any of them don't leave me in check, I'm not in checkmate
    if self.is_check(color):
      return False
    for piece, squares in self.available_moves(color):
      for square in squares:
        dummy_board = deepcopy(self)
        piece_square = self.get_square(piece)
        dummy_board.board[piece_square].move(dummy_board, square, None)
        valid_move = not dummy_board.is_check(color)
        if valid_move:
          return False
    return True